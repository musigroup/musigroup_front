import React, { Component } from 'react';
import './navBar.css';

import logo from '../../assets/images/headphones.svg';

class NavBar extends Component {
    render() {
        return (
            <div className="navbar">
                <img className="logo" src={logo}/>
                <div className="app-name">{this.props.appName}</div>
                <button className="login-btn">login</button>
            </div>
        );
    }
}

export default NavBar;