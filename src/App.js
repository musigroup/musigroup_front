import React, { Component } from 'react';
import './App.css';

import NavBar from './components/navbar/navBar';

class App extends Component {
  render() {
    return (
      <div className="App">
        <NavBar appName="MusiGroup"/>
      </div>
    );
  }
}

export default App;
